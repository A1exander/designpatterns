package com.example.patternssample.creational.prototype

class Student(
    override val age: Int,
    override val gender: String,
    override val height: Int,
    private val lessons: List<String>,
    private val diary: List<String>
) : Human(
    age = age,
    gender = gender,
    height = height
) {

    constructor(student: Student) : this(
        age = student.age,
        gender = student.gender,
        height = student.height,
        lessons = student.lessons,
        diary = student.diary
    )

    override fun clone(): Human = Student(this)

}