package com.example.patternssample.creational.factory_method.device_event_maker

class PhoneScreenEventMaker : InputEventMaker {

    override fun onClick() {
        println("Нажат экран телефона!")
    }

    override fun makeAnimation() {
        println("Рисуется анимация нажатия вокруг точки, где пользователь держит палец")
    }

}