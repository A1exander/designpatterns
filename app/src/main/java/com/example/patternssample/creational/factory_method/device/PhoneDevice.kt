package com.example.patternssample.creational.factory_method.device

import com.example.patternssample.creational.factory_method.device_event_maker.InputEventMaker
import com.example.patternssample.creational.factory_method.device_event_maker.PhoneScreenEventMaker

class PhoneDevice : CommonDevice() {

    override fun interactWithDevice(): InputEventMaker = PhoneScreenEventMaker()

}