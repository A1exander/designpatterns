package com.example.patternssample.creational.factory_method.device

import com.example.patternssample.creational.factory_method.device_event_maker.InputEventMaker

abstract class CommonDevice {

    fun interact() {
        val eventMaker = interactWithDevice()
        eventMaker.onClick()
        eventMaker.makeAnimation()
    }

    protected abstract fun interactWithDevice(): InputEventMaker
}