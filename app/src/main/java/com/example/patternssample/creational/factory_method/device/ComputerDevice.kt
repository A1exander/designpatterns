package com.example.patternssample.creational.factory_method.device

import com.example.patternssample.creational.factory_method.device_event_maker.ComputerMouseEventMaker
import com.example.patternssample.creational.factory_method.device_event_maker.InputEventMaker

class ComputerDevice : CommonDevice() {

    override fun interactWithDevice(): InputEventMaker = ComputerMouseEventMaker()

}