package com.example.patternssample.creational.prototype

class MedicalDoctor(
    override val age: Int,
    override val height: Int,
    override val gender: String,
    private val grade: Boolean,
    private val office: Boolean
) : Human(
    age = age,
    height = height,
    gender = gender
) {

    constructor(medicalDoctor: MedicalDoctor) : this(
        age = medicalDoctor.age,
        height = medicalDoctor.height,
        gender = medicalDoctor.gender,
        grade = medicalDoctor.grade,
        office = medicalDoctor.office
    )

    override fun clone(): Human = MedicalDoctor(this)

}