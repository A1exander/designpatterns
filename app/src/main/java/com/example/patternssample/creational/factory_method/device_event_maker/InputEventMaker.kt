package com.example.patternssample.creational.factory_method.device_event_maker

interface InputEventMaker {

    fun onClick()

    fun makeAnimation()
}