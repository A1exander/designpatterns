package com.example.patternssample.creational.factory_method.device_event_maker

class ComputerMouseEventMaker : InputEventMaker {

    override fun onClick() {
        println("Кликнута мышка!")
    }

    override fun makeAnimation() {
        println("Рисуется анимация нажатия вокруг курсора")
    }
}