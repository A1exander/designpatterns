package com.example.patternssample.creational.prototype

abstract class Human(
    open val age: Int,
    open val gender: String,
    open val height: Int,
) {

    constructor(human: Human) : this(
        age = human.age,
        gender = human.gender,
        height = human.height
    )

    abstract fun clone(): Human

}