package com.example.patternssample.creational.factory_method

import com.example.patternssample.creational.factory_method.device.ComputerDevice
import com.example.patternssample.creational.factory_method.device.PhoneDevice
import kotlin.random.Random

fun main() {
    val startCondition = Random.nextBoolean()

    val device = if (startCondition) PhoneDevice() else ComputerDevice()

    device.interact()
}