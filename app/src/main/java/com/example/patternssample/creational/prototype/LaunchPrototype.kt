package com.example.patternssample.creational.prototype

fun main() {
    val student = Student(
        age = 15,
        gender = "male",
        height = 158,
        lessons = listOf("Алгебра", "Физкультура"),
        diary = listOf("Геометрия: дз", "История: 2")
    )

    val medicalDoctor = MedicalDoctor(
        age = 32,
        height = 188,
        gender = "female",
        grade = true,
        office = true
    )

    println("Is students equals ${student == student.clone()}")
    println("Is medical doctors equals ${medicalDoctor == medicalDoctor.clone()}")
}