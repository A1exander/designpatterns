package com.example.patternssample.behavioral.command

class AddEvenIntegersCommand(private val listOfData: MutableList<Any>) : Command {

    override fun execute() {
        (0..100).forEach {
            if (it % 2 == 0) {
                listOfData.add(it)
            }
        }
    }

}