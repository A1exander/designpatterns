package com.example.patternssample.behavioral.command

interface Command {

    fun execute()

}