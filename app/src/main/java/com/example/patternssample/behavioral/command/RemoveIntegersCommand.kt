package com.example.patternssample.behavioral.command

class RemoveIntegersCommand(private val listOfData: MutableList<Any>) : Command {

    override fun execute() {
        val itemsToRemove = mutableListOf<Int>()
        listOfData.forEach {
            if(it is Int) {
                itemsToRemove.add(it)
            }
        }

        itemsToRemove.forEach {
            listOfData.remove(it)
        }
    }

}