package com.example.patternssample.behavioral.command

fun main() {
    val data = mutableListOf<Any>("ksdfjbskjdfbks", 1, 2, 4, 2123, 435, 6576, 123, "sldfksldf", "sodufhoweiw")

    val addStringsCommand = AddStringsCommand(data)
    execute(addStringsCommand)
    execute(addStringsCommand)

    val addIntegersCommand = AddEvenIntegersCommand(data)
    execute(addIntegersCommand)

    val removeIntegersCommand = RemoveIntegersCommand(data)
    execute(removeIntegersCommand)

    val removeStringsCommand = RemoveStringsCommand(data)
    execute(removeStringsCommand)

    execute(addStringsCommand)
    execute(addIntegersCommand)

    println(data)
}

private fun execute(command: Command) {
    command.execute()
}