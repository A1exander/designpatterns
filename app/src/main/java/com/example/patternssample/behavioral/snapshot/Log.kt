package com.example.patternssample.behavioral.snapshot

class Log(private val states: MutableList<State> = mutableListOf()) {

    fun saveState(state: State) {
        states.add(state)
    }

    fun restorePreviousState(): State? {
        if (states.isNotEmpty()) {
            return states.removeLastOrNull()
        }

        return null
    }
}