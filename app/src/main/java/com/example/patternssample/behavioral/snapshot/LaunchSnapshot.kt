package com.example.patternssample.behavioral.snapshot

fun main() {
    val editor = Editor()
    val log = Log()

    editor.applyString("Here you can find activities to practise your reading skills. ")
    log.saveState(editor.createSnapshot())

    editor.applyString("aljsdlakjsdlakjsdlsak")
    log.restorePreviousState()?.let {
        editor.setCurrentState(it)
    }

    editor.applyString("Reading will help you to improve your understanding of the language and build your vocabulary.")
    log.saveState(editor.createSnapshot())

    editor.logCurrentEditorState()

}