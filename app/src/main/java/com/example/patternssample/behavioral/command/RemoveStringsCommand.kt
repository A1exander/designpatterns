package com.example.patternssample.behavioral.command

class RemoveStringsCommand(private val listOfData: MutableList<Any>) : Command {

    override fun execute() {
        val itemsToRemove = mutableListOf<String>()
        listOfData.forEach {
            if(it is String) {
                itemsToRemove.add(it)
            }
        }

        itemsToRemove.forEach {
            listOfData.remove(it)
        }
    }

}