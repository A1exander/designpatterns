package com.example.patternssample.behavioral.command

import java.util.UUID

class AddStringsCommand(private val listOfData: MutableList<Any>) : Command {

    override fun execute() {
        repeat(100) {
            listOfData.add(UUID.randomUUID().toString())
        }
    }

}