package com.example.patternssample.behavioral.snapshot

class Editor(var stringBuilder: StringBuilder = StringBuilder()) {

    fun createSnapshot() = State(stringBuilder.toString())

    fun applyString(newText: String) {
        stringBuilder.append(newText)
    }

    fun logCurrentEditorState() {
        println(stringBuilder.toString())
    }

    fun setCurrentState(state: State) {
        stringBuilder = StringBuilder(state.string)
    }

}