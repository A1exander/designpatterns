package com.example.patternssample.structural.proxy

class CounterProxy(
    private val counterImpl: CounterImpl
) : Counter {

    override fun onSubscribe() {
        counterImpl.onSubscribe()
        println("A new subscriber was added, current subscribers count: ${counterImpl.subscribersCount}")
    }

    override fun onUnsubscribe() {
        counterImpl.onUnsubscribe()
        println("Subscribers count is decreased, current subscribers count: ${counterImpl.subscribersCount}")
    }

    override fun onDozenSubscribe() {
        counterImpl.onDozenSubscribe()
        println("A new ten subscriber was added, current subscribers count: ${counterImpl.subscribersCount}")
    }

    override fun onSubscriberBanned() {
        counterImpl.onSubscriberBanned()
        println("One subscriber was banned, current subscribers count: ${counterImpl.subscribersCount}")
    }
}