package com.example.patternssample.structural.facade

fun main() {
    val complicatedObject = ComplicatedObject()

    val facadeObject = FacadeObject(complicatedObject)

    facadeObject.launchActivity()
    facadeObject.destroyActivity()
}