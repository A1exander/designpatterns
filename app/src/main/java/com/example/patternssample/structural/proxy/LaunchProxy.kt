package com.example.patternssample.structural.proxy

fun main() {
    val counterImpl = CounterImpl(50)
    val counterProxy = CounterProxy(counterImpl)

    counterProxy.apply {
        onSubscribe()
        onSubscribe()
        onSubscribe()
        onSubscribe()
    }

    counterProxy.onUnsubscribe()

    counterProxy.onDozenSubscribe()

    counterProxy.apply {
        onSubscribe()
        onSubscribe()
        onSubscribe()
    }

    counterProxy.apply {
        onSubscriberBanned()
        onSubscriberBanned()
        onSubscriberBanned()
        onSubscriberBanned()
        onSubscriberBanned()
        onSubscriberBanned()
    }

    counterProxy.apply {
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
        onUnsubscribe()
    }
}