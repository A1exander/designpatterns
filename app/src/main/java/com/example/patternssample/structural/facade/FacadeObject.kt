package com.example.patternssample.structural.facade

class FacadeObject(private val complicatedObject: ComplicatedObject) {

    fun launchActivity() {
        complicatedObject.apply {
            onCreate()
            onStart()
            onResume()
        }
    }

    fun destroyActivity() {
        complicatedObject.apply {
            onPause()
            onStop()
            onDestroy()
        }
    }
}