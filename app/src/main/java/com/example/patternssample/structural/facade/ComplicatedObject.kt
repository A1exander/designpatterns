package com.example.patternssample.structural.facade

class ComplicatedObject {

    fun onCreate() {
        println("Активити не видно")
    }

    fun onStart() {
        println("Активити видно, но недоступно для взаимодействия")
    }

    fun onResume() {
        println("Активити видно и доступно для взаимодействия")
    }

    fun onPause() {
        println("Активити видно, но теряет фокус пользователя")
    }

    fun onStop() {
        println("Активити не видно для пользователя, сохранение незафиксированных данных")
    }

    fun onDestroy() {
        println("Активити уничтожается")
    }

}