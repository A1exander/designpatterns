package com.example.patternssample.structural.proxy

interface Counter {

    fun onSubscribe()

    fun onUnsubscribe()

    fun onDozenSubscribe()

    fun onSubscriberBanned()
}