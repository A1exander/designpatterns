package com.example.patternssample.structural.proxy

class CounterImpl(var subscribersCount: Int) : Counter {

    override fun onSubscribe() {
        subscribersCount++
    }

    override fun onUnsubscribe() {
        subscribersCount--
    }

    override fun onDozenSubscribe() {
        subscribersCount += 10
    }

    override fun onSubscriberBanned() {
        onUnsubscribe()
    }

}